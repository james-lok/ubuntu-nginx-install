username=$1

ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ec2-user@34.242.140.189 << EOF
    echo $username
EOF

# BEGIN{printf "Sr No\tName\tSub\tMarks\n"} 
# /a/ {print $4 "\t" $3}
# {print $3 "\t" $4}
# {print}

# gets the list of processes, filters the ones with sleep in the name and returns only the second column that contains the PIDs
ps aux | awk '/sleep/ {print $2}'