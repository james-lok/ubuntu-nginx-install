echo "Enter your new username"
read username
echo "Enter your new password"
read -s password

echo "Re-enter your new password"
read -s reentered_password

if ! [[ "$reentered_password" == "$password" ]];
then
    echo "passwords don't match"
    exit 1
fi

echo "passwords match"

ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ec2-user@34.242.140.189 << EOF
sudo useradd -m $username 
sudo passwd $username
$password
$password
EOF
