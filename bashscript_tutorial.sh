# bash script for testing arguments and user input

# argument
# data that a function can take in and use internally
# $ touch <argument>
# or 
# $ mv <argument1> <argument2>

# in script is you want to use arguments they are definded with $# , where # is a number.
# for example

echo $1
echo $1

# Interpolation of variable into a strings

echo "this is agument 1: $1"
echo "this is agument 2: $2"
echo "this is agument 3: $3"
echo "this is agument 4: $4"

#Imagine you want to make a function or scipt that takes any number of arguments and does something?

You can use $* to represent all the given argments in a list.

to call this scrip with 4 arguments, just call the scrip and pass each argument separated by spaced


a loop is a block of code that runs for a given number of times, this can be useful if you hace repetitive tasks
for a in 1 2 3 4 5 6 7 8 9 10
do
    # if a = 5 then continue the loop and 
    # don't move to line 8
    if [ $a == 5 ]
    then 
        continue
    fi
    echo "Iteration no $a"
done

## while loops
a=0
# -lt is less than operator
  
#Iterate the loop until a less than 10
while [ $a -lt 10 ]
do 
    # Print the values
    echo $a
      
    # increment the value
    a=`expr $a + 1`
done

until statement
The until loop is executed as many as times the condition/command evaluates to false. The loop terminates when the condition/command becomes true.

a=0
# -gt is greater than operator
  
#Iterate the loop until a is greater than 10
until [ $a -gt 10 ]
do 
    # Print the values
    echo $a
      
    # increment the value
    a=`expr $a + 1`
done

## Escape notation 
In bash and other languages you have certain character that behave signal important things other thatn the actual symbol for example
echo "\$1"
echo "\""

double qoutes protect single quotes and vice versa
echo "You're ok, you're alright"
echo '"Hello, how are you?"'

for movie in "Spirited Away" "Howl's moving castle" "Encanto" "Incredibles" "Hilda"
do
    echo $movie
done

counter = 0
for crazy_landlord in 1 2 3
do
    echo $crazy_landlord
done
echo "You have had $counter crazy landlords!"

counter = 0
for restaurant in "Eat Tokyo" "Duck and Waffle" "Franco Manco"
do
    echo "$counter - $restaurant"
done