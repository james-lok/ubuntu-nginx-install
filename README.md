# Instructions to launch website on another computer
## Prerequisites
* You have cloned this repository using:

```git clone git@bitbucket.org:james-lok/ubuntu-nginx-install.git```

* You have the ip of the chosen omputer
* You have succesfully ssh into the chosen machine at least once

## 1. Make sure you are located in the directory of this repository

``` $ cd path/to/repo```

Type: 

```bash 
$ bash bashscript_install_website.sh 
# To launch a website on AWS, use bashscript_install_website_aws.sh instead of bashscript_install_website.sh 
``` 

Hit enter

## 2. You will be prompted for the ip of the computer you want to connect to

``` Hello, what is the ip you want to copy to? ``` 

## 3. Enter your ip and press enter

The webpage should open showing a personlised page

# Introduction to bashscipt coding

Bash Script, Variables, Arguements, and Conditions

This will be a small demo/ code along for use to learn how to use variables and conditions.

By the end we'll  have a small script that uses these to install ngix on a ubuntu machine when given an IP as an argument

We'll talk about the difference between arguments and variables.

We will also look into using conditions, to help us run our script - using conditions, to help us run our script - using control flow and handling errors.


## Running scripts
to run a script, it must first be exefccutable. check it's permissions using `ll` or `ls -l`. it should have `x` to be executed.

Add the permissions "execute" by `chmod +x <file>`

you might need to do this in a remote machine as well when moving or creating new files on the fly.

you can run the file by just pointing to it or calling bash <file>

```bash
# pointing to file to run it
./<file>

# to run file remotely using bash
$ bash <path/to/file>
```

ssh -o StrictHostKeyChecking=no -i <path/to/key> <name>@<ip> '
list of commands

'

testing jenkins
