# Bashscript to run website on AWS
echo "Hello, what is the ip you want to copy to?"
read varname
scp -i  ~/.ssh/ch9_shared.pem -r james_website ec2-user@$varname:
scp -i  ~/.ssh/ch9_shared.pem bashscript_install_aws.sh ec2-user@$varname:
ssh -i ~/.ssh/ch9_shared.pem ec2-user@$varname bash bashscript_install_aws.sh
open http://$varname/