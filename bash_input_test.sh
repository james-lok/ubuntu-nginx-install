if [ $# != 2 ]
then
    echo "Expected one argument, $count was given instead"
    exit 1
fi

key=$1
hostname=$2

if ! output=$(ssh -o StrictHostKeyChecking=no -i $key ec2-user@$hostname sudo httpd -v)
then
    echo "httpd could not be found"
    ssh -o StrictHostKeyChecking=no -i $key ec2-user@$hostname '
    sudo yum update
    sudo yum install httpd -y
    '
fi
scp -i  $key -r james_website ec2-user@$hostname:
ssh -o StrictHostKeyChecking=no -i $key ec2-user@$hostname '
sudo mv ~/james_website/index.html /var/www/html/index.html
'
ssh -o StrictHostKeyChecking=no -i $key ec2-user@$hostname sudo service httpd restart

# What happend if I dont give argument (hostname)? 
# create a if condition that checks that script has been called with argument
# if it has, set it to hostname, else let the user know to call the script with a ip end the script (exit code 1) 

# ssh -o StrictHostKeyChecking=no -i $key ubuntu@$hostname '

# sudo apt update

# sudo apt install nginx -y
# # Create an is condition when you check if nginx already installed. 
# # if installed do nothing 
# # else install 

# sudo systemctl start nginx 
# '


# output=`ls`
# for x in $output
# do 
# echo $x
# done